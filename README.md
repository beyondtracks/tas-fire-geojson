# :fire: tas-alerts-geojson

Tasmanian Alerts from https://alert.tas.gov.au as GeoJSON.

## Hosted Data

A hosted and processed feed is at [https://www.beyondtracks.com/contrib/tas-alerts.geojson](https://www.beyondtracks.com/contrib/tas-alerts.geojson) - [preview with geojson.io](http://geojson.io/#data=data:text/x-url,https://www.beyondtracks.com/contrib/tas-alerts.geojson). This service is available on a best effort basis with no SLA.

# Usage

Install the Node dependencies with:

    yarn install

Run the script with:

  ./bin/tas-alerts-geojson --pretty-print --output tas-alerts.geojson

## Options

* `--pretty-print` pretty print the output, otherwise output is minified
* `--output` output file, otherwise written to stdout

## Legal Notices

THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
