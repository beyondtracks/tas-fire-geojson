module.exports = {
  convert(input, options) {
    //console.log(JSON.stringify(input.incidents, null, 2))
    //console.log(JSON.stringify(input.cap, null, 2))

    let incidents
    if (input.incidents && input.incidents.rss & input.incidents.rss.channel && input.incidents.rss.channel.item && input.incidents.rss.channel.item.length) {
      incidents = input.incidents.rss.channel.item.map(item => {
        return {
          title: item.title._text,
          link: item.link._text,
          description: item.description._text,
          pub_date: item.pubDate._text,
          creator: item['dc:creator']._text,
          guid: item.guid._text
        }
      })
    }

    let features
    if (
      input.cap &&
      input.cap.EDXLDistribution &&
      input.cap.EDXLDistribution.contentObject &&
      input.cap.EDXLDistribution.contentObject.length
    ) {
      features = input.cap.EDXLDistribution.contentObject.map((entry) => {
        const properties = {}
        let geometry

        if (
          entry.xmlContent &&
          entry.xmlContent.embeddedXMLContent &&
          entry.xmlContent.embeddedXMLContent.alert &&
          entry.xmlContent.embeddedXMLContent.alert.info
        ) {
          const info = entry.xmlContent.embeddedXMLContent.alert.info
          if (info.area) {
            let point
            let polygon
            let multiPolygon

            if (
              info.area.circle &&
              info.area.circle._text
            ) {
              const [xy, r] =
                info.area.circle._text.split(" ")
              const [lat, lng] = xy.split(",").map(i => Number(i))
              point = {
                type: "Point",
                coordinates: [lng, lat]
              }
              properties.radius = Number(r)
            }

            if (
              info.area.polygon &&
              info.area.polygon._text
            ) {
              const ring =
                info.area.polygon._text.split(" ")
                .map(ll => ll.split(',').reverse().map(i => Number(i)))
              polygon = {
                type: "Polygon",
                coordinates: [ring]
              }
            }

            if (
              info.area.polygon && info.area.polygon.length
            ) {
              const coordArrays = info.area.polygon.map(polygon => {
                if (polygon._text) {
                  const ring = polygon._text.split(' ')
                    .map(ll => ll.split(',').reverse().map(i => Number(i)))

                  return [ring]
                } else {
                  return null
                }
              })
                .filter(coordArray => !!coordArray)

              if (coordArrays.length) {
                multiPolygon = {
                  type: 'MultiPolygon',
                  coordinates: coordArrays
                }
              }
            }

            if (point && (polygon || multiPolygon)) {
              geometry = {
                type: "GeometryCollection",
                geometries: [point, polygon || multiPolygon]
              }
            } else if (point) {
              geometry = point
            } else if (polygon) {
              geometry = polygon
            }
          }

          for (const [k, v] of Object.entries(info)) {
            if (v?._text) {
              properties[k] = v._text
            } else if (v?.value?._text) {
              properties[k] = v.value._text
            }
          }

          if (info.parameter && info.parameter.length) {
            for (const parameter of info.parameter) {
              if (parameter?.valueName?._text && parameter?.value?._text) {
                properties[parameter.valueName._text] = parameter.value._text
              }
            }
          }

          if (info?.resource?.uri?._text) {
            properties.uri = info.resource.uri._text
          }
        }

        if (options.noGeometryCollections && geometry?.type === 'GeometryCollection') {
          return geometry.geometries.map(g => {
            return {
              type: 'Feature',
              properties,
              geometry: g
            }
          })
        } else {
          return [{
            type: 'Feature',
            properties,
            geometry
          }]
        }
      })
    }

    return {
      type: 'FeatureCollection',
      features: features.flat()
    }
  }
}
